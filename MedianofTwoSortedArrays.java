import java.util.*;

class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int totalLength = nums1.length + nums2.length;
        if (totalLength % 2 == 0) {
            return (findKthElement(nums1, nums2, totalLength / 2) + findKthElement(nums1, nums2, totalLength / 2 + 1)) / 2.0;
        } else {
            return findKthElement(nums1, nums2, totalLength / 2 + 1);
        }
    }
    
    private int findKthElement(int[] nums1, int[] nums2, int k) {
        int index1 = 0, index2 = 0;
        while (true) {
            if (index1 == nums1.length) {
                return nums2[index2 + k - 1];
            }
            if (index2 == nums2.length) {
                return nums1[index1 + k - 1];
            }
            if (k == 1) {
                return Math.min(nums1[index1], nums2[index2]);
            }
            
            int newIndex1 = Math.min(index1 + k / 2, nums1.length) - 1;
            int newIndex2 = Math.min(index2 + k / 2, nums2.length) - 1;
            
            if (nums1[newIndex1] <= nums2[newIndex2]) {
                k -= (newIndex1 - index1 + 1);
                index1 = newIndex1 + 1;
            } else {
                k -= (newIndex2 - index2 + 1);
                index2 = newIndex2 + 1;
            }
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of nums1 array: ");
        int m = scanner.nextInt();
        int[] nums1 = new int[m];
        System.out.println("Enter the elements of nums1 array:");
        for (int i = 0; i < m; i++) {
            nums1[i] = scanner.nextInt();
        }
        System.out.print("Enter the size of nums2 array: ");
        int n = scanner.nextInt();
        int[] nums2 = new int[n];
        System.out.println("Enter the elements of nums2 array:");
        for (int i = 0; i < n; i++) {
            nums2[i] = scanner.nextInt();
        }
        
        Solution solution = new Solution();
        double median = solution.findMedianSortedArrays(nums1, nums2);
        System.out.println("Median of the two sorted arrays: " + median);
    }
}
